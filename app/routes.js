import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Photos from './containers/Photos';
import Activities from './containers/Activities';

export default (
	<Switch>
		<Route exact path="/" component={Photos} />
		<Route path="/activities" component={Activities} />
	</Switch>
);
