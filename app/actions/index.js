import * as types from './types';

export function userRequest() {
    return {
        type: types.USER_INFO_REQUEST,
    };
}

export function userSuccess(payload) {
    return {
        type: types.USER_INFO_SUCCESS,
        payload
    };
}

export function userFailure(payload) {
    return {
        type: types.USER_INFO_FAILURE,
        payload
    };
}

export function userFetch() {
    const fields = { fields: 'name, cover, email, picture.type(large), likes.limit(10){name,picture}, photos.type(uploaded).limit(10){name,images,picture}' };

    return dispatch => {
        dispatch(userRequest());
        window.FB.api('/me', fields, (meResponse) => {
            if (!meResponse || meResponse.error) {
                dispatch(userFailure(meResponse));
            } else {
                dispatch(userSuccess(meResponse));
            }
        });
    };
}

