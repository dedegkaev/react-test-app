import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { configureStore, history } from './store/configureStore';
import Root from './containers/Root';
import './styles/common.scss';
const store = configureStore();

window.FB.init({
    appId: '2513924272080009',
    autoLogAppEvents: true,
    xfbml: true,
    version: 'v2.10'
});

window.FB.login((response) => {
    if (response.authResponse) {
        render(
            <AppContainer>
                <Root store={store} history={history} />
            </AppContainer>,
            document.getElementById('root')
        );

        if (module.hot) {
            module.hot.accept('./containers/Root', () => {
                const newConfigureStore = require('./store/configureStore');
                const newStore = newConfigureStore.configureStore();
                const newHistory = newConfigureStore.history;
                const NewRoot = require('./containers/Root').default;
                render(
                    <AppContainer>
                        <NewRoot store={newStore} history={newHistory} />
                    </AppContainer>,
                    document.getElementById('root')
                );
            });
        }
    } else {
        window.location.reload();
    }
}, {
    scope: 'user_likes, user_photos',
    auth_type: 'rerequest'
});
