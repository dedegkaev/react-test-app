import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';
import * as types from '../actions/types';

const user = (state = {isFailure: false, isFetching: false, info: undefined}, action) => {
    switch (action.type) {
        case types.USER_INFO_REQUEST:
            return Object.assign({}, state, { isFetching: true });
        case types.USER_INFO_SUCCESS:
            return Object.assign({}, state, { isFetching: false, info: action.payload });
        case types.USER_INFO_FAILURE:
            return Object.assign({}, state, { isFetching: false, isFailure: true, error: action.payload, info: undefined  });
        default:
            return state;
    }
};

const rootReducer = combineReducers({
    user,
    routing
});

export default rootReducer;
