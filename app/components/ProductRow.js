import PropTypes from 'prop-types';
import React from 'react';
import { tableItem, tableItemPhotos } from '../styles/tableItem.scss';

const ProductRow = ({ data, type }) =>
    <div className={type === 'horizontal' ? tableItem : tableItemPhotos}>
        { data.picture.data && <img src={data.picture.data.url} /> }
        { data.images && <img src={data.images[0].source} /> }
        { data.name && <p>{data.name}</p> }
    </div>;

ProductRow.propTypes = {
    data: PropTypes.object,
    type: PropTypes.string
};

export default ProductRow;
