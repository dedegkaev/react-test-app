import PropTypes from 'prop-types';
import React from 'react';
import ProductRow from './ProductRow';

const ItemsTable = ({ items, tableType }) => {
    let rows = [];
    items.forEach(item => {
        rows.push(
            <ProductRow type={tableType} key={item.id} data={item} />
        );
    });
    return <div> {rows} </div>;
};

ItemsTable.propTypes = {
    items: PropTypes.array,
    tableType: PropTypes.string
};

export default ItemsTable;
