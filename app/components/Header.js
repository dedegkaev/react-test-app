import PropTypes from 'prop-types';
import React from 'react';
import { header } from '../styles/header.scss';

const Header = ({user}) =>
    <div>
        <div className={header} style={ user.info && { backgroundImage: `url(${user.info.cover.source})` }}>
            { user.isFetching && <div>Loading...</div> }
            { user.info && <div className="header__user-name">{user.info.name}</div> }
            { user.info && <img src={user.info.picture.data.url} /> }
        </div>
    </div>;

Header.propTypes = {
    user: PropTypes.object
};

export default Header;
