import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { tabs, selected } from '../styles/tabs.scss';
import { overlay } from '../styles/overlay.scss';
import { app } from '../styles/app.scss';
import Header from '../components/Header';
import Routes from '../routes';
import { userFetch } from '../actions';

class App extends Component {
    componentDidMount() {
        this.props.fetchUser();
    }
    render() {
        return (
            <div className={app}>
                <Header user={this.props.user} />
                <div className={tabs}>
                    <NavLink exact activeClassName={selected} to="/">Photos</NavLink>
                    <NavLink exact activeClassName={selected} to="/activities">Likes</NavLink>
                </div>
                { Routes }
                <div className={overlay}/>
            </div>
        );
    }
}

App.propTypes = {
    user: PropTypes.object,
    fetchUser: PropTypes.func
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchUser: () => dispatch(userFetch())
    };
};
const mapStateToProps = (state) => {
    return {
        user: state.user
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
