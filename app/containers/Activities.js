import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import ItemsTable from '../components/ItemsTable';
import { table } from '../styles/table.scss';

class Activities extends Component {
    render() {
        return (
            <div className={table}>
                { this.props.user && <ItemsTable tableType={'horizontal'} items={this.props.user.likes.data} /> }
            </div>
        );
    }
}

Activities.propTypes = {
    user: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        user: state.user.info
    };
};

export default connect(
    mapStateToProps
)(Activities);
